package game2019;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class Client extends Application {

	public static final int size = 20;
	public static final int scene_height = size * 20 + 100;
	public static final int scene_width = size * 20 + 200;

	public static Image image_floor;
	public static Image image_wall;
	public static Image hero_right, hero_left, hero_up, hero_down;

	public static List<Player> players = new ArrayList<Player>();
	public static ServerCommunicationThread serverThread;

	private static Label[][] fields;
	private TextArea scoreList;
	
	private Player playerMe;

	private String[] board = { // 20x20
			"wwwwwwwwwwwwwwwwwwww",
			"w        ww        w",
			"w w  w  www w  w  ww",
			"w w  w   ww w  w  ww",
			"w  w               w",
			"w w w w w w w  w  ww",
			"w w     www w  w  ww",
			"w w     w w w  w  ww",
			"w   w w  w  w  w   w",
			"w     w  w  w  w   w",
			"w ww ww        w  ww",
			"w  w w    w    w  ww",
			"w        ww w  w  ww",
			"w         w w  w  ww",
			"w        w     w  ww",
			"w  w              ww",
			"w  w www  w w  ww ww",
			"w w      ww w     ww",
			"w   w   ww  w      w",
			"wwwwwwwwwwwwwwwwwwww"
	};

	// -------------------------------------------
	// | Maze: (0,0)              | Score: (1,0) |
	// |-----------------------------------------|
	// | boardGrid (0,1)          | scorelist    |
	// |                          | (1,1)        |
	// -------------------------------------------

	public static void main(String[] args) throws Exception {
		launch(args);
	}

	public void runClient() {
		Socket clientSocket;
		try {
			clientSocket = new Socket("127.0.0.1", 6969);
			DataOutputStream messageToServer = new DataOutputStream(clientSocket.getOutputStream());
			BufferedReader messageFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			playerMe = new Player("Kirsten", 3, 3, "up");
			serverThread = new ServerCommunicationThread(this, messageToServer, messageFromServer);
			serverThread.start();
			serverThread.joinGame(playerMe);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start(Stage primaryStage) {
		try {
			GridPane grid = new GridPane();
			grid.setHgap(10);
			grid.setVgap(10);
			grid.setPadding(new Insets(0, 10, 0, 10));

			Text mazeLabel = new Text("Maze:");
			mazeLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			Text scoreLabel = new Text("Score:");
			scoreLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

			scoreList = new TextArea();

			GridPane boardGrid = new GridPane();

			image_wall = new Image(getClass().getResourceAsStream("Image/wall4.png"), size, size, false, false);
			image_floor = new Image(getClass().getResourceAsStream("Image/floor1.png"), size, size, false, false);

			hero_right = new Image(getClass().getResourceAsStream("Image/heroRight.png"), size, size, false, false);
			hero_left = new Image(getClass().getResourceAsStream("Image/heroLeft.png"), size, size, false, false);
			hero_up = new Image(getClass().getResourceAsStream("Image/heroUp.png"), size, size, false, false);
			hero_down = new Image(getClass().getResourceAsStream("Image/heroDown.png"), size, size, false, false);

			fields = new Label[20][20];
			for (int j = 0; j < 20; j++) {
				for (int i = 0; i < 20; i++) {
					switch (board[j].charAt(i)) {
					case 'w':
						fields[i][j] = new Label("", new ImageView(image_wall));
						break;
					case ' ':
						fields[i][j] = new Label("", new ImageView(image_floor));
						break;
					default:
						throw new Exception("Illegal field value: " + board[j].charAt(i));
					}
					boardGrid.add(fields[i][j], i, j);
				}
			}
			scoreList.setEditable(false);

			grid.add(mazeLabel, 0, 0);
			grid.add(scoreLabel, 1, 0);
			grid.add(boardGrid, 0, 1);
			grid.add(scoreList, 1, 1);

			Scene scene = new Scene(grid, scene_width, scene_height);
			primaryStage.setScene(scene);
			primaryStage.show();

			scene.addEventFilter(KeyEvent.KEY_PRESSED, event -> {
				switch (event.getCode()) {
				case UP: {
					sendDirection(0, -1, "up");
					break;
				}
				case DOWN: {
					sendDirection(0, +1, "down");
					break;
				}
				case LEFT: {
					sendDirection(-1, 0, "left");
					break;
				}
				case RIGHT: {
					sendDirection(+1, 0, "right");
					break;
				}
				default:
					break;
				}
			});

			scoreList.setText(getScoreList());
		} catch (Exception e) {
			e.printStackTrace();
		}

		runClient();
	}

	public void movePlayer(String playerName, int delta_x, int delta_y, String direction) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				Player current = findPlayer(playerName);
				current.direction = direction;
				int x = current.getXpos(), y = current.getYpos();
				if (board[y + delta_y].charAt(x + delta_x) == 'w') {
					current.addPoints(-1);
				} else {
					Player p = getPlayerAt(x + delta_x, y + delta_y);
					if (p != null) {
						current.addPoints(10);
						p.addPoints(-10);
					} else {
						current.addPoints(1);

						fields[x][y].setGraphic(new ImageView(image_floor));
						x += delta_x;
						y += delta_y;

						if (direction.equals("right")) {
							fields[x][y].setGraphic(new ImageView(hero_right));
						}
						;
						if (direction.equals("left")) {
							fields[x][y].setGraphic(new ImageView(hero_left));
						}
						;
						if (direction.equals("up")) {
							fields[x][y].setGraphic(new ImageView(hero_up));
						}
						;
						if (direction.equals("down")) {
							fields[x][y].setGraphic(new ImageView(hero_down));
						}
						;

						current.setXpos(x);
						current.setYpos(y);
					}
				}
				scoreList.setText(getScoreList());
			}
		});

	}

	public String getScoreList() {
		StringBuffer b = new StringBuffer(100);
		for (Player p : players) {
			b.append(p + "\r\n");
		}
		return b.toString();
	}

	public Player getPlayerAt(int x, int y) {
		for (Player p : players) {
			if (p.getXpos() == x && p.getYpos() == y) {
				return p;
			}
		}
		return null;
	}

	public void sendDirection(int x, int y, String direction) {
		serverThread.sendMovement(playerMe.name, x, y, direction);
	}

	public Player findPlayer(String playerName) {
		for (Player p : players) {
			if (p.name.equals(playerName)) {
				return p;
			}
		}
		return null;
	}

	public void addPlayer(String playerName, int posX, int posY, String direction) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				if (findPlayer(playerName) == null) {
				Player playerNew = new Player(playerName, posX, posY, direction);
				players.add(playerNew);
				fields[posX][posY].setGraphic(new ImageView(hero_up));
			}
		}
	});
}
}