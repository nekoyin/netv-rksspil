package game2019;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;

public class ServerCommunicationThread extends Thread {

	private Client client;
	private DataOutputStream messageToServer;
	private BufferedReader messageFromServer;

	public ServerCommunicationThread(Client client, DataOutputStream messageToServer,
			BufferedReader messageFromServer) {
		this.client = client;
		this.messageToServer = messageToServer;
		this.messageFromServer = messageFromServer;
	}

	@Override
	public void run() {
		while (true) {
			try {
				String message = messageFromServer.readLine();
				String status = message.substring(0, message.indexOf(" "));
				if (status.equals("moved")) {
					String[] playerInfo = message.split(" ");
					client.movePlayer(playerInfo[1], Integer.parseInt(playerInfo[2]), Integer.parseInt(playerInfo[3]),
							playerInfo[4]);
				} else if (status.equals("joined")) {
					String[] playerInfo = message.split(" ");
					client.addPlayer(playerInfo[1], Integer.parseInt(playerInfo[2]), Integer.parseInt(playerInfo[3]),
							playerInfo[4]);
				} else {
					String[] playerList = message.substring(message.indexOf(" ") + 1).split("\\|");
					for (String s : playerList) {
						String[] playerInfo = s.split(" ");
						client.addPlayer(playerInfo[0], Integer.parseInt(playerInfo[1]),
								Integer.parseInt(playerInfo[2]),
								playerInfo[3]);
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void sendMovement(String playerName, int x, int y, String direction) {
		try {
			messageToServer.writeBytes(playerName + " " + x + " " + y + " " + direction + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void joinGame(Player player) {
		//Ingen forskel mellem joinGame og sendMovement lige nu.
		sendMovement(player.name, player.xpos, player.ypos, player.direction);
	}
}