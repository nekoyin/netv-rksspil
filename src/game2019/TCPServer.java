package game2019;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class TCPServer {

	//private static List<ClientCommunicationThread> clients = new ArrayList<ClientCommunicationThread>();
	private static Game game = new Game();

	public static void main(String[] args) throws Exception {
		ServerSocket welcomeSocket = new ServerSocket(6969);

		while (true) {
			Socket connectionSocket = welcomeSocket.accept();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(connectionSocket.getOutputStream());
			String clientMessage = inFromClient.readLine();
			ClientCommunicationThread newClient = new ClientCommunicationThread(connectionSocket, outToClient,
					inFromClient, game);
			newClient.start();
			String[] playerInfo = clientMessage.split(" ");
			game.addNewPlayer(newClient, playerInfo[0], Integer.parseInt(playerInfo[1]),
					Integer.parseInt(playerInfo[2]), playerInfo[3]);
		}
	}
}