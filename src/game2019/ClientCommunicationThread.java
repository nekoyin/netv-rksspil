package game2019;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class ClientCommunicationThread extends Thread {

	private Socket socket;
	private DataOutputStream messageToClient;
	private BufferedReader messageFromClient;
	private Game game;

	public ClientCommunicationThread(Socket socket, DataOutputStream messageToClient, BufferedReader messageFromClient,
			Game game) {
		this.socket = socket;
		this.messageToClient = messageToClient;
		this.messageFromClient = messageFromClient;
		this.game = game;
	}

	public void run() {
		while (true) {
			try {
				String[] playerInfo = messageFromClient.readLine().split(" ");
				System.out.println(playerInfo);
				game.moveUser(playerInfo[0], Integer.parseInt(playerInfo[1]), Integer.parseInt(playerInfo[2]),
						playerInfo[3]);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void updateClient(String playerName, int x, int y, String direction) {
		try {
			messageToClient.writeBytes("moved" + " " + playerName + " " + x + " " + y + " " + direction + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addNewPlayer(Player player) {
		try {
			messageToClient.writeBytes("joined" + " " + player.name + " " + player.xpos + " " + player.ypos + " " + player.direction + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sendPlayers(List<Player> players) {
		String playerMessage = "players ";
		for (Player p : players) {
			playerMessage += p.name + " " + p.xpos + " " + p.ypos + " " + p.direction + "|";
		}
		playerMessage += "\n";
		try {
			messageToClient.writeBytes(playerMessage);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
