package game2019;

import java.util.ArrayList;
import java.util.List;

public class Game {
	private List<ClientCommunicationThread> clientThreads;
	private List<Player> players;

	public Game() {
		this.clientThreads = new ArrayList<ClientCommunicationThread>();
		this.players = new ArrayList<Player>();
	}

	public void moveUser(String playerName, int x, int y, String direction) {
		for (ClientCommunicationThread t : clientThreads) {
			t.updateClient(playerName, x, y, direction);
		}
	}

	public void addNewPlayer(ClientCommunicationThread newClient, String playerName, int x, int y, String direction) {
		Player player = new Player(playerName, x, y, direction);
		players.add(player);
		newClient.sendPlayers(players);
		this.clientThreads.add(newClient);
		for (ClientCommunicationThread t : clientThreads) {
			t.addNewPlayer(player);
		}
	}
}